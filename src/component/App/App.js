import React from 'react';
import PropTypes from 'prop-types';
import {
    Provider as ReduxProvider
} from 'react-redux';



class App extends React.PureComponent {

    constructor(props){
        super(props);
    }
      
    render() {

        return <ReduxProvider  {...this.props.context}>
                {this.props.children}
        </ReduxProvider>
    }
}

export default App;
