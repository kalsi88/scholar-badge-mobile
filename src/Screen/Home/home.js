import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash'
import { IMAGE_URLS } from '../../config/URLconfig';
import {View,Text,Image,StyleSheet,ImageBackground,ProgressBarAndroid} from 'react-native'
import { fontSizes , colors} from '../../config/ui';
import Button from '../../component/Button/button';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { showLoader } from '../../util/coreUtils';

class Home extends React.Component{

    static navigationOptions = {
        title: 'Home',
        headerStyle: {
            backgroundColor: colors.BLUE,
          },
          headerTintColor: '#fff',
        headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: fontSizes.LARGE,
        textAlign: 'center',
        opacity: .4,
        flexGrow:1,
        alignSelf:'center',
    },

      };
    constructor(props){
        super(props);
        this.state = {};

    }
    
    toGame  = async () => {
        this.props.navigation.navigate('TestSelection');

    }
    render() {
        const {
            rootData,
        } = this.props;
        return (
            <View style = {{flex: 1}}>
                <ImageBackground source={IMAGE_URLS.baseBG}  style = {s.mainContainer}>
                    
                    <View style = {s.badgeCard}>
                        <Image style = {{height: 30,alignSelf: 'center',paddingVertical:5}} source = {IMAGE_URLS.ROLL_OF_HONOUR}/>
                        <View style = {{flexDirection: 'row',justifyContent:'space-around'}}>
                            <ImageBackground imageStyle= {{width:70,}} style = {{height: 80,width:70,justifyContent:'center',alignItems:'center'}} source = {IMAGE_URLS.PLANE_GOLD}>
                                <Text style = {{fontSize:30,fontWeight:'600',paddingBottom:10}}>2</Text>
                            </ImageBackground>
                            <ImageBackground imageStyle= {{width:70,}} style = {{height: 80,width:70,justifyContent:'center',alignItems:'center'}} source = {IMAGE_URLS.PLANE_SILVER}>
                                <Text style = {{fontSize:30,fontWeight:'600',paddingBottom:10}}>2</Text>
                            </ImageBackground>
                            <ImageBackground imageStyle= {{width:70,}} style = {{height: 80,width:70,justifyContent:'center',alignItems:'center'}} source = {IMAGE_URLS.PLANE_BRONZE}>
                                <Text style = {{fontSize:30,fontWeight:'600',paddingBottom:10}}>2</Text>
                            </ImageBackground>
                        </View>
                    </View>

                    <View style = {s.standingCard}>
                        <Image style = {{height: 30,alignSelf: 'center',paddingVertical:5}} source = {IMAGE_URLS.MY_STANDING}/>
                       
                        <ProgressBarAndroid
                            styleAttr="Horizontal"
                            indeterminate={false}
                            progress={0.5}
                            style = {{height:10}}
                            color ="#66cbff"
                            />
                    </View>

                    <TouchableOpacity style = {s.playButton} onPress = {this.toGame}>
                        <Image style ={{height:50,width:150}} source = {IMAGE_URLS.PLAY}></Image>
                    </TouchableOpacity>
                </ImageBackground>
            </View>
        );

    }


}

const s = StyleSheet.create({
    mainContainer: {
        padding: 25,
        height: '100%',
    },
    badgeCard : {
        backgroundColor : colors.PINK,
        padding: 10,
        borderRadius:10,
        borderColor: '#eb5c78',
        borderWidth:1,
        marginTop:60,
        elevation: 5
    },
    standingCard: {
        backgroundColor : colors.GREEN_BLUE,
        padding: 10,
        borderRadius:10,
        marginTop:60,
        elevation: 5
    },
    playButton: {
        elevation: 5,
        backgroundColor: colors.PLAY_YELLOW,
        borderRadius: 10,
        padding:10,
        width:300,
        height:70,
        alignItems:'center',
        marginTop:60,
        alignSelf: 'center'

    }

})
const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = {
    
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
