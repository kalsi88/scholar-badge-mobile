import {ACTION_TYPES} from "../constant/constants";
import _ from 'lodash';


function dcData(state = {}, { type, payload }) {
    let data;
    switch (type) {
        case ACTION_TYPES.GET_NOTES:
            data = _.get(state,payload.mappedChapterId,{});
            data['notes'] = payload.data;
            return {...state,[payload.mappedChapterId]:data};
        case ACTION_TYPES.SET_CONTNET:
            data = _.get(state,payload.mappedChapterId,{});
            data['content'] = payload.data;
            return {...state,[payload.mappedChapterId]:data};
        case ACTION_TYPES.SET_EXTRA:
            data = _.get(state,payload.mappedChapterId,{});
            data['extra'] = payload.data;
            return {...state,[payload.mappedChapterId]:data};
        case ACTION_TYPES.SET_NCERT:
            data = _.get(state,payload.mappedChapterId,{});
            data['ncert'] = payload.data;
            return {...state,[payload.mappedChapterId]:data};
        default:
            return state;
    }
};

export default dcData;
