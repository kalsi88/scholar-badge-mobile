import { IMAGE_URLS } from "./URLconfig";

export const ALL_STRING_CONSTANT = {
    LoginScreenTitle: 'MCQ Gaming',
    searchPlaceHolder: "Search",
    title: "Scholar Badge",
    digitalContentTitle: "Digital Contents",
    mindMapTitle: "Mind Mapping",
    loginText: "Log in to Scholar Badge",
    loginPageText: "Login to Scholar Badge",
    forgetPassword: "Forget Password?",
    signUpTitle: "Join Scholar Badge",
    subTitleSignUp: "whatever you're into it's all free on Scholar Badge",
    signUpTextLogin: 'Don\'t have an account?',
    ROLL_OF_HONOR: "Roll Of Honor",
    myStanding: "My Standing"
}


export const dynamicPageConfig = [
    {
        title: ALL_STRING_CONSTANT.loginPageText,
        _id: "HOME_PAGE",
        key: "home",
        route: "/mcq/home",
        logo: IMAGE_URLS.HOME_ICON
    },
    {
        title: ALL_STRING_CONSTANT.loginPageText,
        _id: "ranking_PAGE",
        key: "ranking",
        route: "/mcq/ranking",
        logo: IMAGE_URLS.RANKING_ICON
    },
    {
        title: ALL_STRING_CONSTANT.loginPageText,
        _id: "GAME_PAGE",
        key: "game",
        route: "/mcq/game",
        logo: IMAGE_URLS.GAME_ICON
    },
    {
        title: ALL_STRING_CONSTANT.loginPageText,
        _id: "STANDING_PAGE",
        key: "standing",
        route: "/mcq/standing",
        logo: IMAGE_URLS.STANDING_ICON
    },
    {
        title: ALL_STRING_CONSTANT.loginPageText,
        _id: "profile_PAGE",
        key: "profile",
        route: "/user/profile",
        logo: IMAGE_URLS.PROFILE_ICON
    },
]
export const sectionConfig = [
    {
        name: "A",
        _id: 0
    },
    {
        name: "B",
        _id: 1
    },
    {
        name: "C",
        _id: 2
    },
    {
        name: "D",
        _id: 3
    },
    {
        name: "Other",
        _id: 99
    }
]

export const genderConfig = [
    {
        name: "Boy",
        _id: 0
    },
    {
        name: "Girl",
        _id: 1
    },
]

export const contentOption = [
    {
        id: 'notes',
        text: 'Passbook',
        action: 'getNotesData',
        color: '#d50f25'
    },
    {
        id: 'content',
        text: 'Content',
        action: 'getContentData',
        color: '#3369e8'
    },
    {
        id: 'ncert',
        text: 'NCERT',
        action: 'getNcertData',
        color: '#009925'
    },
    {
        id: 'extra',
        text: 'Extra Miles',
        action: 'getExtraData',
        color: '#fbb305'
    }
];

export const  accessOptions = [
    {
        textHead: "G",
        text: "Guest",
        textSub: "Explore",
        color : "#d50f25",
        link: "selection/explore"
    },
    {
        textHead: "U",
        text: "User",
        textSub: "Log in",
        color : "#3369e8",
        link: "selection/login"

    },
    {
        textHead: "N",
        text: "Create Account",
        textSub: "Sign up",
        color : "#009925",
        link: "selection/signup"
    }
];