import React from 'react';
import { connect } from 'react-redux';
import { IMAGE_URLS } from '../../config/URLconfig';
import { View, Text, Image, StyleSheet, ImageBackground } from 'react-native'
import { fontSizes, colors } from '../../config/ui';
import Button, { buttonSize, buttonType } from '../../component/Button/button'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
class PostGame extends React.Component {

    static navigationOptions = {
        title: 'Home',
        headerStyle: {
            backgroundColor: colors.BLUE,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: fontSizes.LARGE,
            textAlign: 'center',
            opacity: .4,
            flexGrow: 1,
            alignSelf: 'center',
        },

    };
    constructor(props) {
        super(props);
        this.state = {};

    }


    changeHandler = (key, value) => {
        this.setState({ [key]: value });
    }
    render() {
        const {
            rootData,
        } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <View style={s.banner}>
                    <Text style={{ fontSize: fontSizes.Small }}>School Name</Text>
                    <Text style={{ fontSize: fontSizes.Small }}>CLASS</Text>
                </View>
                <ImageBackground source={IMAGE_URLS.baseBG} style={s.mainContainer}>
                    <Text style={{ fontSize: fontSizes.Small, paddingBottom: 10 }} >Chapter - 1 : Crop Production and Management</Text>

                    <View style={s.mainCard}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View>
                                <Text style={[s.textCenter, { fontSize: fontSizes.Small,marginTop:15 }]}>C1T1 Completed</Text>
                                <Image style={[s.textCenter, { width: 120, height: 120 }]} source={IMAGE_URLS.GirlImage} />
                                <Text style={[s.textCenter]}>Millie</Text>
                                <Text style={[s.textCenter, s.biggestFont]}>Won <Text style={[s.textBold, s.biggestFont]}>GOLD</Text></Text>
                                <Image style={{ width: 140, alignSelf: 'center',marginVertical:15 }} source={IMAGE_URLS.WON_GOLD} />
                                <Text style={[s.fontSmall, s.textBold, s.textCenter]}>Score : <Text style={s.fontSmall}>20/20</Text></Text>
                                <View style={{ flexDirection: 'row', marginTop: 15, padding: 15 }}>

                                    <View style={{ flex: .7, flexDirection: 'column' }} >
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={s.statusBullets}></View>
                                            <Text style={s.textBold}>Unattempted : <Text>0/20</Text></Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={s.statusBullets}></View>
                                            <Text>Correct : <Text>20/20</Text></Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={s.statusBullets}></View>
                                            <Text>Incorrect : <Text>0/20</Text></Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: .4 }}>
                                        <Button type={buttonType.ORANGE} size={buttonSize.MEDIUM}>Review</Button>
                                        <Button style={{ marginTop: 5 }} type={buttonType.YELLOW} size={buttonSize.MEDIUM}>Next Test</Button>
                                    </View>


                                </View>
                                <View style={{
                                    marginTop: 10, backgroundColor: '#f6f6f8', padding: 15,
                                    borderBottomLeftRadius: 15, borderBottomRightRadius: 15
                                }}>
                                    <Text>Share your Score with your friends and family</Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
                                        <TouchableOpacity><Image style={s.shareIcon} source={IMAGE_URLS.W_APP} /></TouchableOpacity>
                                        <TouchableOpacity><Image style={s.shareIcon} source={IMAGE_URLS.FACEBOOK} /></TouchableOpacity>
                                        <TouchableOpacity><Image style={s.shareIcon} source={IMAGE_URLS.Twitter} /></TouchableOpacity>
                                        <TouchableOpacity><Image style={s.shareIcon} source={IMAGE_URLS.INSTA} /></TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </ImageBackground>
            </View>
        );

    }


}

const s = StyleSheet.create({
    mainContainer: {
        padding: 25,
        height: '100%',
    },
    mainCard: {
        elevation: 5,
        backgroundColor: 'white',
        marginTop: 15,
        height: '85%',
        borderRadius: 15
    },
    textCenter: {
        alignSelf: 'center'
    },
    textBold: {
        fontWeight: '600'
    },
    biggestFont: {
        fontSize: 30
    },
    fontSmall: {
        fontSize: fontSizes.Small
    },
    banner: {
        backgroundColor: '#f3fafe',
        padding: 10,
        flexDirection: 'row',
        paddingHorizontal: 25,
        justifyContent: 'space-between'
    },
    shareIcon: {
        width: 50,
        height: 50
    },
    statusBullets: {
        width: 15,
        height: 15,
        marginRight: 15,
        marginVertical: 5,
        backgroundColor: 'red'
    }

})
const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(PostGame);
