import React from 'react';
import { connect } from 'react-redux';
import { IMAGE_URLS } from '../../config/URLconfig';
import { View, Text, Image, StyleSheet, ImageBackground } from 'react-native'
import { fontSizes, colors } from '../../config/ui';
import Button, { buttonSize } from '../../component/Button/button'
import McqCard from '../../component/McqCard/mcqcard';
import { TouchableOpacity } from 'react-native-gesture-handler';
import _ from 'lodash'

class Game extends React.Component {

    static navigationOptions = {
        title: 'Home',
        headerStyle: {
            backgroundColor: colors.BLUE,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: fontSizes.LARGE,
            opacity: .4,
            alignSelf:'center'
        },
        headerRight:()=><View style={{width:80}}/>

    };
    constructor(props) {
        super(props);
        this.state = { questionNo: 0, ansData: {} };
        this.params = this.props.navigation.state.params;
    }



    arrowClicked = (isLeft) => {
        const qlistLenght = _.get(this.params.data, 'qList', []).length;
        let newQNo;
        if (isLeft) {
            if (this.state.questionNo != 0) {
                newQNo = --this.state.questionNo;
            }
        } else {
            if (this.state.questionNo < (qlistLenght - 1)) {
                newQNo = ++this.state.questionNo;
            }
        }
        if (typeof (newQNo) !== 'undefined') {
            this.setState({
                questionNo: newQNo
            })
        }
    }
    optionSelected = (qid, oid) => {
        let ansData = this.state.ansData;
        ansData[qid] = oid;
        this.setState({
            ansData
        })
    }
    isOptionSelected = (qid, oid) => this.state.ansData[qid] == oid


    submitClicked = () => {
        if (this.state.afterPlay) {
            return
        }
        // let ansIdList = this.props.mcqData.qList.map(data => {
        //     if (this.state.ansData[data._id]) {
        //         return this.state.ansData[data._id]
        //     }
        //     return this.props.mcqData.testId;
        // })
        // const bundle = {
        //     ansIdList,
        //     schoolName: _.get(this.props.userData, 'school', ''),
        //     sectionAndGrade: `${_.get(this.grade, 'name', '')} ${_.get(this.props.userData, 'section', '')}`,
        //     chapterTitle: this.chapterTitle,
        //     testName: this.props.testName,
        //     mcqData: this.props.mcqData
        // }
        this.props.navigation.navigate("PostGame",{})
    }


    render() {
        const qList = this.params.data.qList;
        return (
            <View style={{ flex: 1 }}>
                <View style={s.banner}>
                    <Text style={{ fontSize: fontSizes.Small }}>School Name</Text>
                    <Text style={{ fontSize: fontSizes.Small }}>CLASS</Text>
                </View>
                <ImageBackground source={IMAGE_URLS.baseBG} style={s.mainContainer}>
                    <Text style={{ fontSize: fontSizes.Small, paddingBottom: 10 }} >{this.params.c_name}</Text>
                    <Text style={{ fontSize: fontSizes.LARGE, alignSelf: 'center', paddingBottom: 5 }} >{this.props.testName}</Text>
                    <McqCard qNo={this.state.questionNo} oClicked={this.optionSelected} data={qList[this.state.questionNo]} selectedOption  ={this.state.ansData[qList[this.state.questionNo]._id]}></McqCard>
                    <View style={s.navigator}>
                        <TouchableOpacity onPress={() => this.arrowClicked(true)}>
                            <Image source={(this.state.questionNo === 0) ? IMAGE_URLS.GreyArrowLeft : IMAGE_URLS.BlueArrowRight}></Image>
                        </TouchableOpacity>
                        <Button onClick={this.submitClicked} style={{ width: 200 }} size={buttonSize.MEDIUM}>SUBMIT</Button>
                        <TouchableOpacity onPress={() => this.arrowClicked(false)}>
                            <Image source={(this.state.questionNo === 19) ? IMAGE_URLS.GreyArrowLeft : IMAGE_URLS.BlueArrowRight}></Image>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        );

    }


}

const s = StyleSheet.create({
    mainContainer: {
        padding: 25,
        height: '100%',
    },
    navigator: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20
    },
    banner: {
        backgroundColor: '#f3fafe',
        padding: 10,
        flexDirection: 'row',
        paddingHorizontal: 25,
        justifyContent: 'space-between'
    }

})
const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(Game);
