import React from 'react';
import {
  AsyncStorage,
  StatusBar,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { getRootData, refreshData } from '../action/CommonActions';
import { TokenProvider } from '../util/createFetch';


class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    if(userToken){
      const ut = JSON.parse(userToken);
      TokenProvider.setToken(ut);
      const root = this.props.getRootData();
      const details = this.props.refreshData();
      this.props.screenProps.showLoader(true);
      await Promise.all([root,details]);
      this.props.screenProps.showLoader(false);
      this.props.navigation.navigate('App');
    }else{
      this.props.screenProps.showLoader(true);
      await this.props.getRootData()
      this.props.screenProps.showLoader(false);
      this.props.navigation.navigate('Auth');
    }
    
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <StatusBar barStyle="default" />
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return{}
};

const mapDispatchToProps = {
  getRootData,
  refreshData
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen);