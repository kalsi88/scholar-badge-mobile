import {ACTION_TYPES} from "../constant/constants";
import _ from 'lodash';


function selectedData(state = {}, { type, payload }) {
    switch (type) {
        case  ACTION_TYPES.SET_SELECTION_DATA:
            return {...state,...payload};
        case ACTION_TYPES.SET_USER_DETAILS:
            if(_.get(payload,'data.status',-1) > -1){
                const gradeID = _.get(payload,'data.data.class');
                return {...state,gradeID}
            }
            return state
        default:
            return state;
    }
};

export default selectedData;