export const BadgeRules = {
    GOLD: {
        score: 20
    },
    SILVER: {
        score: 19
    },
    BRONZE: {
        score: 18
    },
}