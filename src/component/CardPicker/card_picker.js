import React, { Component } from 'react';
import {View,Picker,StyleSheet} from 'react-native';
import { fontSizes, colors } from '../../config/ui';


class CardPicker extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        return ( 

            <View style = {[s.picker,this.props.style]}>
                 <Picker
                    style = {{fontSize: fontSizes.MEDIUM}}
                selectedValue={this.props.value}
                onValueChange={(itemValue, itemIndex) =>
                    {
                    if(itemIndex == 0){
                        return
                    }
                    this.props.onChange(...itemValue)
                }
                }>
                 <Picker.Item color = {colors.textInputBorderColor} key = {'placeholder'} label={this.props.placeholder} value={''} />
                {
                    this.props.list.map((data) => {
                        return <Picker.Item key = {data._id} label={data.name} value={[data._id,data]} />
                    })
                }
            </Picker>
            </View>

         );
    }
}
 
const s = StyleSheet.create({
    picker: {
       backgroundColor:'#fafafa',
       borderRadius:5,
       elevation: 5,
       paddingHorizontal:10
    }
});
export default CardPicker;