
import {ACTION_TYPES} from "../constant/constants";
import _ from 'lodash';


function testData(state = {}, { type, payload }) {
    switch (type) {
        case ACTION_TYPES.SET_TEST_DATA:
            const testMap = _.get(payload,'testData',{});
            return {...state,testMap};
        default:
            return state;
    }
};

export default testData;