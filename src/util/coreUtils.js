export const checkHtml = (data) => {
    return data;
};

export const jsonToFormEncorded = (json) => {
    let string = "";
    for(let key in json) {
        string += `${key}=${json[key]}&`
    }
    return string;
}


export const quaryParser = (data) => {
    return data.slice(1).split('&').reduce((old,pairs) => {
        const split = pairs.split('=');
        old[split[0]] = split[1]
        return old
    },{})
}


export const showLoader = async (_this,flag) => {
   await  _this.props.screenProps.showLoader(flag)
}