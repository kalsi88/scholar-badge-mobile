import React, { Component } from 'react';
import { colors } from '../config/ui';
import {ActivityIndicator,View} from 'react-native';
class Loader extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
            <View style = {{width:"100%",
            height:'100%',backgroundColor: 'black',
            position: 'absolute',
            opacity: .7,
            alignItems: 'center',
            justifyContent: 'center'
            }}>
                <ActivityIndicator size= {100} color={colors.accentBlue} />
            </View>
          );
    }
}
 
export default Loader;