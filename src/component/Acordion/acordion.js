import React, { Component } from 'react';
import {View,StyleSheet,Text,NativeModules,Image,Transforms} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { colors } from '../../config/ui';
import { IMAGE_URLS } from '../../config/URLconfig';
import _ from 'lodash'


const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);
class Acordion extends Component {
    constructor(props) {
        super(props);
        this.state = {collapsed:true,tests: []}
    }
    toggle = () => {
        if(this.state.collapsed){
            this.setState({collapsed:false});
            this.collapseClicked()
        }else{
            this.setState({collapsed:true});
        }
    }


    collapseClicked = async () => {
        let tests = this.state.tests;
        if(!this.state.testChapterData){
            await this.props.shwoLoader(true)
           const responce =  await this.props.fetchTestData(this.props.id);
           await this.props.shwoLoader(false)

           const count = this.props.testData.testCount;
           const playedTest = _.get(responce,'data.tests',[]).map((data)=>{
               return {id:data.id,played:true,active: false,score: data.score}
           })
           tests = tests.concat(playedTest);
           if(tests.length < count)
           {
               tests.push({id: "tempidFirst",played:false,active: true,score: false})
           }
           const futureTests = Array(count-tests.length).fill({played:false,active: false,score: false});
           tests = tests.concat(futureTests)
        }

        let rows = [];
        for(let i = 1;i<=tests.length;i = i+4){
            rows.push(tests.slice(i-1,i+3))
        }
        
        this.setState({tests:rows });
        
    }
    badgeClicked = (data) =>{
        this.props.badgeClicked(data);
    }





    render() { 
        const {
            name,
            number
        } = this.props;
        return (  
            <View style = {s.wrapper}>
                <TouchableOpacity onPress = {this.toggle} style = {s.header}>
                    <Text>Chapter {number} {name}</Text>
                    <Image style = {
                        {
                            width: 20,
                            height: 20,
                            position: 'absolute',
                            right:20,
                            top: 10,
                            transform: [{rotate: this.state.collapsed? '0deg':'180deg'}]
                        }
                    } source = {IMAGE_URLS.ArrowDown}></Image>
                </TouchableOpacity>
                <View style = {[s.innerWrapperView,{display: this.state.collapsed?'none': 'flex'}]}>
                    
                {

                    this.state.tests.map((row,rowNo)=>{
                        return (
                            <View key = {rowNo} style = {s.row}>
                                {
                                    row.map((data,colNO) => {
                                        return (
                                            <TouchableOpacity key = {colNO} onPress = {()=> {data.active && this.badgeClicked({...data,testName: `C${number}T${Number(rowNo * 4) + Number(Number(colNO) +1)}`,c_name:`Chapter ${number} ${name}`})}} activeOpacity = {data.active? .2:1}>
                                            {
                                                data.score ? 
                                                (Number(data.score) == BadgeRules.GOLD) ? <Image source = {IMAGE_URLS.GOLD_BADGE_ICON}/> :
                                                (Number(data.score) == BadgeRules.SILVER) ? <Image source = {IMAGE_URLS.SILVER_BADGE_ICON}/>:
                                                (Number(data.score) == BadgeRules.BRONZE) ? <Image source = {IMAGE_URLS.BRONZE_BADGE_ICON}/>:
                                                <Image source = {IMAGE_URLS.ACTIVE_BADGE_ICON}/>
                                                :
                                                data.active? 
                                                <Image source = {IMAGE_URLS.ACTIVE_BADGE_ICON}/>:
                                                <Image source = {IMAGE_URLS.INACTIVE_BADGE_ICON}/>
                                            } 
                                                <Text style = {{alignSelf:'center'}}>C{number}T{Number(rowNo * 4) + Number(Number(colNO) +1)}</Text>
                                            </TouchableOpacity>
                                        )

                                    })
                                }
                                
                                
                            </View>
                        )
                    })

                    }








                </View>
            </View>
        );
    }
}

const s = StyleSheet.create({
    wrapper: {
        borderColor: '#d7d7de',
        borderWidth: 1,
        borderRadius: 5,
        marginVertical:10
    },
    header: {
        backgroundColor: '#d7d7de',
        padding: 10,
        paddingHorizontal:20,
        flexDirection: 'row'
    },
    innerWrapperView: {
        padding: 10,
        paddingHorizontal:30 ,
        backgroundColor: colors.WHITE
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical:10,
    }
})
export default Acordion;