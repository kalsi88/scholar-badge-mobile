import { combineReducers } from 'redux';
import dcData from "./digitalContent";
import CommonData from "./commonData";
import selectedData from './selectedData';
import testData from './testData';


export default combineReducers({
    dcData,
    CommonData,
    selectedData,
    testData
});
