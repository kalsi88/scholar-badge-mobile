



const base = "https://fast-reaches-79297.herokuapp.com/api";

const host = base;



export const API_URLS ={
    GET_NOTES: `${host}/dc/get/notes`,
    DO_LOGIN: `${host}/user/login`,
    DO_SIGNUP: `${host}/user/register`,
    CONTENT: `${host}/dc/get/content`,
    NCERT: `${host}/dc/get/ncert`,
    EXTRA: `${host}/dc/get/extra`,
    GET_ROOT: `${host}/root`,
    GET_DETAILS: `${host}/user/getDetails`,
    GET_CHAPTER_TEST_DETAILS: `${host}/mcq-test/getTestDetailForChapter`,
    PLAY_TEST: `${host}/mcq-test/playTest3`,
    SUBMIT_TEST: `${host}/mcq-test/submitTest3`

};