import React, { Component } from 'react';
import {TouchableOpacity,Text} from 'react-native';
import { colors, fontSizes } from '../../config/ui';

export const buttonType ={
    SUCCESS: {buttonColor: colors.SUCCESS, textColor: '#ffff'},
    PRIMARY: {buttonColor: colors.accentBlue, textColor: '#ffff'},
    YELLOW: {buttonColor: colors.YELLOW, textColor: '#000000'},
    ORANGE: {buttonColor: colors.ORANGE, textColor: '#000000'}

}
export const buttonSize = {
    LARGE: {text: fontSizes.MEDIUM,padding: 15},
    MEDIUM: {text: fontSizes.MEDIUM,padding: 10}
}

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const styleButton = {
            padding: this.props.size.padding,
            backgroundColor: this.props.type.buttonColor,
            borderRadius: 5
        }
        const styleButtonText = {
            fontSize: this.props.size.text,
            color: this.props.type.textColor,
            alignSelf: 'center'
        }

        return ( 

            <TouchableOpacity style = {[styleButton,this.props.style]} onPress = {this.props.onClick}>
                <Text style = {styleButtonText} >{this.props.children}</Text>
            </TouchableOpacity>
         );
    }
}

Button.defaultProps = {
    type: buttonType.PRIMARY,
    size: buttonSize.LARGE
}
 
export default Button;