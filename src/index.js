 import React from 'react';
import { INITIAL_STATE } from './config/Initstate';
import createFetch from "./util/createFetch";
import configureStore from "./store/configureStore";
import App from './component/App/App';
import getCookie from './util/getCookie';
import AppNavigator from './routes'
import Loader from './component/Loader'
import { resolveUri } from '../node_modules/expo-asset/build/AssetSources';

const context = {
        
    fetch: createFetch(fetch, getCookie),
    // Initialize a new Redux store
    store: configureStore(INITIAL_STATE, {
        fetch: createFetch(fetch, getCookie),
    }),
    storeSubscription: null,
};

export default class AndroidIndex extends React.Component {

    constructor(p){
        super(p)
        this.state = {showLoader: false}
    }

    showLoader = async (isShow) =>  {
        const _this = this;
        if(this.state.showLoader !== isShow){
            return new Promise((resolveUri,rej) => {
                _this.setState({showLoader: isShow},() => {resolveUri()})
            }) 
        }
    } 
    render () {
        return (
            <App context = {context}>
                <AppNavigator screenProps = {{showLoader:this.showLoader}}/>
                { this.state.showLoader && <Loader></Loader>}
            </App>
            
        )
    }
    
}