import {ACTION_TYPES} from "../constant/constants";
import _ from 'lodash';


function CommonData(state = {}, { type, payload }) {
    switch (type) {
        case ACTION_TYPES.GET_ROOT_DATA:
            const grades = _.get(payload,'gradeData.list',[]);
            return {...state,grades};
        case ACTION_TYPES.SET_USER_DETAILS:
            return {...state,user: payload.data};
        default:
            return state;
    }
};

export default CommonData;