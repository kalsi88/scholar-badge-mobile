import {API_URLS} from "../constant/apiURL";
import _  from 'lodash'
import {ACTION_TYPES} from "../constant/constants";

export function getNotesData(mappedChapterId = "5c65281addd92d00041d821a"){

    return async (dispatch, getState,{fetch}) => {

        try{
            const state = getState();
            if(_.has(state,`dcData.${mappedChapterId}.notes`)){
                return;
            }
            const url = API_URLS.GET_NOTES;
            const responce = await fetch(url, {
                body: `mappedChapterId=${mappedChapterId}`,
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });
            dispatch({
                type: ACTION_TYPES.GET_NOTES,
                payload : {mappedChapterId,data:_.get(responce,'data',{})}
            })
        }
        catch (e) {
            console.log(e)
        }

    }
}

function getCompleteQuestionList(data) {
    let questionList = [];
    for (let notes of data) {
        questionList = questionList.concat(notes.contentList);
        if (notes.childList) {
            if (notes.childList.length > 0) {
                questionList = questionList.concat(getCompleteQuestionList(notes.childList));
            }
        }
    }
    return questionList;
}
export function getContentData(mappedChapterId){

    return async (dispatch, getState,{fetch}) => {

        try{
            await getNotesData()(dispatch,getState,{fetch});
            const state = getState();
            if(_.has(state,`dcData.${mappedChapterId}.content`)){
                return;
            }
            const list = getCompleteQuestionList(_.get(state,`dcData.${mappedChapterId}.notes.childList`,[]));
            const url = API_URLS.CONTENT;
            const responce = await fetch(url, {
                body: `mappedChapterId=${mappedChapterId}&contentList=${JSON.stringify(list)}`,
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });
            dispatch({
                type: ACTION_TYPES.SET_CONTNET,
                payload : {mappedChapterId,data:_.get(responce,'data',{})}
            })
        }
        catch (e) {
            console.log(e)
        }

    }
}



export function getExtraData(mappedChapterId){

    return async (dispatch, getState,{fetch}) => {

        try{
            const state = getState();
            if(_.has(state,`dcData.${mappedChapterId}.extra`)){
                return;
            }
            const url = API_URLS.EXTRA;
            const responce = await fetch(url, {
                body: `mappedChapterId=${mappedChapterId}`,
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });
            dispatch({
                type: ACTION_TYPES.SET_EXTRA,
                payload : {mappedChapterId,data:_.get(responce,'data',{})}
            })
        }
        catch (e) {
            console.log(e)
        }

    }
}



export function getNcertData(mappedChapterId){

    return async (dispatch, getState,{fetch}) => {

        try{
            const state = getState();
            if(_.has(state,`dcData.${mappedChapterId}.ncert`)){
                return;
            }
            const url = API_URLS.NCERT;
            const responce = await fetch(url, {
                body: `mappedChapterId=${mappedChapterId}`,
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });
            dispatch({
                type: ACTION_TYPES.SET_NCERT,
                payload : {mappedChapterId,data:_.get(responce,'data',{})}
            })
        }
        catch (e) {
            console.log(e)
        }

    }
}


