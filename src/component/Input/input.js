import { TextInput, StyleSheet } from "react-native";
import React, { Component } from 'react';
import { colors } from "../../config/ui";


class Input extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        return ( 
            <TextInput 
                style={[s.defaultTextInput,this.props.style]}
                onChangeText={(text) => this.props.onChange(text)}
                value={this.props.value}
                editable = {this.props.editable}
                maxLength = {this.props.max}
                autoFocus = {this.props.autoFocus}
                placeholder ={this.props.placeholder}
                keyboardType ={this.props.keyboardType}
                autoCompleteType = {this.props.autoCompleteType}
                textContentType = {this.props.textContentType}
                returnKeyType = {this.props.returnKeyType}
                secureTextEntry = {this.props.secureTextEntry}

            />
         );
    }
}

Input.defaultProps ={
    value: '',
    textContentType: 'none',
    editable: true,
    max: 40,
    autoCompleteType: 'off',
    autoFocus: false,
    placeholder: '',
    keyboardType: 'default',
    returnKeyType:'next',
    onChange: () =>{},
    secureTextEntry: false
}

const s = StyleSheet.create({
    defaultTextInput: {
        padding: 10,
        backgroundColor: colors.textInputBgColor,
        borderColor: colors.textInputBorderColor,
        borderWidth:2,
        borderRadius:5,
        marginBottom:10
        
    },
})
 
export default Input;