import React, { Component } from 'react';
import {Picker,StyleSheet,View} from 'react-native';
import { colors } from '../../config/ui';

class DropDown extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        return ( 
            <View style = {[s.picker,this.props.style]}>
                 <Picker
                
                selectedValue={this.props.value}
                onValueChange={(itemValue, itemIndex) =>
                    this.props.onChange(itemValue,itemIndex)
                }>
                    <Picker.Item color = {colors.textInputBorderColor} key = {'placeholder'} label={this.props.placeholder} value={''} />

                {
                    this.props.list.map((data) => {
                        return <Picker.Item key = {data.key} label={data.name} value={data.key} />
                    })
                }
            </Picker>
            </View>
           
         );
    }
}
 
const s = StyleSheet.create({
    picker: {
        backgroundColor: colors.textInputBgColor,
        borderColor: colors.textInputBorderColor,
        borderWidth:2,
        borderRadius:5,
        marginBottom:10,
        flex:1
    }
});
export default DropDown;