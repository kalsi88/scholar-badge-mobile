import React, { Component } from 'react';
import {View,StyleSheet,TouchableOpacity,Text} from 'react-native';
import { WebView } from 'react-native-webview';

class McqCard extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        return ( 
            <View style = {{flex: .9}}>
                <View style = {s.questionCard}>
                    <Text style = {{paddingTop : 4,fontSize:18}}>{this.props.qNo+1}.</Text>
                    <WebView
                        style = {{flex:1,backgroundColor: 'transparent'}}
                        originWhitelist={['*']}
                        source = {{html:'<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">'+this.props.data.ques}}/>
                </View>
                <TouchableOpacity onPress = {()=>{this.props.oClicked(this.props.data._id,this.props.data.optionList[0]._id)}} style = {[s.optionCard,this.props.selectedOption == this.props.data.optionList[0]._id ? {backgroundColor:'green'} : {}]}>
                    <Text style = {{paddingTop : 4,fontSize:18}}>A.</Text>
                    <WebView
                        style = {{flex:1,backgroundColor: 'transparent'}}
                        originWhitelist={['*']}
                        source = {{html:'<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">'+this.props.data.optionList[0].text}}/>
                </TouchableOpacity>
                <TouchableOpacity onPress = {()=>{this.props.oClicked(this.props.data._id,this.props.data.optionList[1]._id)}} style = {[s.optionCard,this.props.selectedOption == this.props.data.optionList[1]._id ? {backgroundColor:'green'} : {}]}>
                    <Text style = {{paddingTop : 4,fontSize:18}}>B.</Text>
                    <WebView
                        style = {{flex:1,backgroundColor: 'transparent'}}
                        originWhitelist={['*']}
                        source = {{html:'<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">'+this.props.data.optionList[1].text}}/>
                </TouchableOpacity>
                <TouchableOpacity onPress = {()=>{this.props.oClicked(this.props.data._id,this.props.data.optionList[2]._id)}} style = {[s.optionCard,this.props.selectedOption == this.props.data.optionList[2]._id ? {backgroundColor:'green'} : {}]}>
                     <Text style = {{paddingTop : 4,fontSize:18}}>C.</Text>
                    <WebView
                        style = {{flex:1,backgroundColor: 'transparent'}}
                        originWhitelist={['*']}
                        source = {{html:'<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">'+this.props.data.optionList[2].text}}/>
                </TouchableOpacity>
                <TouchableOpacity onPress = {()=>{this.props.oClicked(this.props.data._id,this.props.data.optionList[3]._id)}} style = {[s.optionCard,this.props.selectedOption == this.props.data.optionList[3]._id ? {backgroundColor:'green'} : {}]}>
                    <Text style = {{paddingTop : 4,fontSize:18}}>D.</Text>
                    <WebView
                        style = {{flex:1,backgroundColor: 'transparent'}}
                        originWhitelist={['*']}
                        source = {{html:'<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">'+this.props.data.optionList[3].text}}/>
                </TouchableOpacity>
                
            </View>
         );
    }
}

const s = StyleSheet.create({
    questionCard: {
        flex:.4,
        backgroundColor: '#f6f6f8',
        borderColor:'#d7d7de',
        borderRadius: 5,
        borderWidth: 1,
        marginBottom:10,
        padding: 10,
        flexDirection: 'row'
    },
    optionCard: {
        flexDirection: 'row',
        flex:.15,
        backgroundColor: '#d7d7de',
        borderRadius: 5,
        elevation:10,
        padding: 10,
        marginBottom:10
    }
})
 
export default McqCard;