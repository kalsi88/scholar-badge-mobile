import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash'
import {doSignup} from "../../action/CommonActions";
import { ALL_STRING_CONSTANT } from '../../config/stringConfig';
import { IMAGE_URLS } from '../../config/URLconfig';
import {View,Text,Image,Picker,StyleSheet,ScrollView,KeyboardAvoidingView,DatePickerAndroid,ToastAndroid} from 'react-native'
import { fontSizes , colors} from '../../config/ui';
import Input from '../../component/Input/input';
import { Header } from 'react-navigation';
import DropDown from '../../component/DropDown/dropdown';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Button, { buttonType } from '../../component/Button/button';
import { showLoader } from '../../util/coreUtils';

class SignupLandingPage extends React.Component{

    static navigationOptions = {
        title: 'SignIn',
        headerStyle: {
            backgroundColor: colors.BLUE,
          },
          headerTintColor: '#fff',
        headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: fontSizes.LARGE,
        textAlign: 'center',
        opacity: .4,
        flexGrow:1,
        alignSelf:'center',
    },

      };
    constructor(props){
        super(props);
        this.state = {};

    }

    submit = async (data) => {
        showLoader(this,true)
        const resp = await this.props.doSignup(data);
        showLoader(this,false)
        if(Number(_.get(resp,'status',-500)) > -1){
            ToastAndroid.show('Congratulations you have successfully registered!', ToastAndroid.SHORT);
            this.props.navigation.navigate('SignIn');
        }else{
            alert("Something went wrong");
        }
    };

    launchDatePicker = async () => {
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
              // Use `new Date()` for current date.
              // May 25 2020. Month 0 is January.
              date: new Date(),
            });
            if (action !== DatePickerAndroid.dismissedAction) {
              this.setState({dob:`${day}/${month+1}/${year}`})
            }
          } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
          }
    }
    changeHandler = (key,value) => {
        this.setState({[key]: value});
    }
    render() {
        const {
            rootData,
        } = this.props;
        return (
            <View style = {s.mainContainer}>
                <Image
                    source={IMAGE_URLS.LOGO_URL}
                    style = {s.image}
                ></Image>
                <Text style = {s.titleText}>{ALL_STRING_CONSTANT.signUpTitle}</Text>
                <Text style = {s.subTitelText}>{ALL_STRING_CONSTANT.subTitleSignUp}</Text>
                <KeyboardAvoidingView 
                    keyboardVerticalOffset = {Header.HEIGHT + 40} // adjust the value here if you need more padding
                    style = {{ flex: 1 }}
                    behavior = "padding"
                >
                    <ScrollView
                        showsVerticalScrollIndicator = {false}
                    >
                        <Input  
                            placeholder = {"Full Name"} 
                            textContentType  ={'name'} 
                            onChange = {(value) => {this.changeHandler('name',value)}}
                            value = {this.state.name}
                            autoFocus = {true}
                            ></Input>
                        <Input
                            placeholder = {"Username"} 
                            textContentType  ={'username'} 
                            onChange = {(value) => {this.changeHandler('username',value)}}
                            value = {this.state.username}
                        ></Input>
                        <Input
                            placeholder = {"Password"} 
                            textContentType  ={'newPassword'} 
                            secureTextEntry = {true}
                            onChange = {(value) => {this.changeHandler('password',value)}}
                            value = {this.state.password}
                        ></Input>
                        <Input
                            placeholder = {"Confirm Password"} 
                            textContentType  ={'newPassword'} 
                            onChange = {(value) => {this.changeHandler('confirmPassword',value)}}
                            value = {this.state.confirmPassword}
                        ></Input>
                        <Input
                            placeholder = {"Email"} 
                            textContentType  ={'emailAddress'} 
                            keyboardType ={'email-address'}
                            onChange = {(value) => {this.changeHandler('email',value)}}
                            value = {this.state.email}
                        ></Input>
                        <Input
                            placeholder = {"Mobile No."} 
                            textContentType  ={'telephoneNumber'} 
                            keyboardType ={'number-pad'}
                            onChange = {(value) => {this.changeHandler('mobile',value)}}
                            value = {this.state.mobile}
                        ></Input>
                        <View style = {s.twoWrapper}>   
                            <DropDown 
                                onChange = {(value) => {this.changeHandler('class',_.get(value,'_id'))}}
                                value = {_.find(this.props.rootData,['_id',this.state.class],'')}
                                placeholder = {'grade'} style = {{marginRight: 10}} list = {this.props.rootData.map((data) => {
                                return {name: data.name,key:data}
                            })}/>
                            <DropDown  placeholder = {'section'}
                                onChange = {(value) => {this.changeHandler('section',value)}}
                                value = {this.state.section}
                            list = {[{name: 'A',key: 'a'},
                                               {name: 'B',key: 'b'},
                                               {name: 'C',key: 'c'},
                                               {name: 'D',key: 'd'}]}/>
                        </View>
                        <View style = {s.schoolName}>
                            <Input
                                placeholder = {"School Name"}
                                onChange = {(value) => {this.changeHandler('school',value)}}
                                value = {this.state.school}
                            ></Input>
                        </View>
                        <View>
                            <Text style = {s.pitext}>Personal Info.</Text>
                            <View style = {s.twoWrapper}>
                                <TouchableOpacity style = {s.dobPicker} onPress = {this.launchDatePicker}>
                                    <Text style = {s.dobText}>{this.state.dob || <Text style = {s.dobPlace}>Birthday dd/mm/yyyy</Text>}</Text>
                                </TouchableOpacity>
                                <DropDown
                                    onChange = {(value) => {this.changeHandler('gender',value)}}
                                    placeholder = {'Select one'}
                                    value = {this.state.gender}
                                list = {[{name: 'Male',key: 'M'},
                                               {name: 'Female',key: 'F'}]}/>
                            </View>
                        </View>
                        <Button style = {{marginVertical: 10}} type = {buttonType.SUCCESS} onClick = {() => {this.submit(this.state)}}>Sign Up</Button>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );

    }


}

const s = StyleSheet.create({
    mainContainer: {
        padding: 25,
        flex:1
    },
    bottomButton: {
        borderTopWidth:1,
        borderTopColor: colors.textInputBorderColor,
        padding: 10,
        position: 'absolute',
        bottom: 0,
        width: '100%',
    },
    image : {
        alignSelf: 'center',
        height: 120,
        width: 120
    },
    titleText: {
        alignSelf: 'center',
        fontWeight: '600',
        paddingHorizontal: 10,
        fontSize: fontSizes.MEDIUM
    },
    subTitelText: {
        alignSelf: 'center',
        paddingBottom: 20,
        fontSize: fontSizes.Small
    },
    twoWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    schoolName: {
        paddingTop: 10,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        marginBottom: 10,
        borderColor: '#cbcaca'
    },
    dobPicker:{
        padding: 10,
        backgroundColor: colors.textInputBgColor,
        borderColor: colors.textInputBorderColor,
        borderWidth:2,
        borderRadius:5,
        marginBottom:10,
        marginRight: 10,
        height: 55,
        
    },
    dobText: {
        textAlign: 'center'
    },
    dobPlace: {
        color:'#a4aab2',
        alignItems: 'center',
        fontSize: fontSizes.Small
    },
    pitext: {
        padding:5,
        fontWeight: '600',
        paddingHorizontal:10,
        fontSize: fontSizes.Small
    }
})

const mapStateToProps = (state) => {
    return {rootData: state.CommonData.grades}
};

const mapDispatchToProps = {
    doSignup
};

export default connect(mapStateToProps, mapDispatchToProps)(SignupLandingPage);
