import {API_URLS} from "../constant/apiURL";
import {ACTION_TYPES} from "../constant/constants";
import _ from "lodash";
import { jsonToFormEncorded } from "../util/coreUtils";

export function getTestDataForChapter(mappedChapterId){

    return async (dispatch, getState,{fetch}) => {
        try{
            const url = API_URLS.GET_CHAPTER_TEST_DETAILS;
            const responce = await fetch(url, {
                body: `chapterId=${mappedChapterId}`,
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });
            return responce
        }
        catch (e) {
            console.log(e)
        }

    }
}


export function startA_NewTest(mappedChapterId){

    return async (dispatch, getState,{fetch}) => {
        try{
            const url = API_URLS.PLAY_TEST;
            const responce = await fetch(url, {
                body: `chapterId=${mappedChapterId}&testType=1`,
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });
            return responce
        }
        catch (e) {
            console.log(e)
        }

    }
}

export function submitTest(data){

    return async (dispatch, getState,{fetch}) => {
        try{
            const url = API_URLS.SUBMIT_TEST;
            const responce = await fetch(url, {
                body: jsonToFormEncorded(data),
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });
            return responce
        }
        catch (e) {
            console.log(e)
        }

    }
}