import {API_URLS} from "../constant/apiURL";
import {ACTION_TYPES} from "../constant/constants";
import _ from "lodash";
import {jsonToFormEncorded} from '../util/coreUtils'

export function setSelectionData(data){

    return   (dispatch) => {
            dispatch({
                type: ACTION_TYPES.SET_SELECTION_DATA,
                payload :data
            })

    }
}


export function setSelectedUserData(data){

    return   (dispatch) => {
            dispatch({
                type: ACTION_TYPES.SET_SELECTION_DATA,
                payload :data
            })

    }
}


export function getRootData(){

    return async (dispatch, getState,{fetch}) => {

        try{

            const state = getState();
            if(!_.isEmpty(_.get(state,"CommonData.grades",[]))){
                return;
            }
            const url = API_URLS.GET_ROOT;
            const responce = await fetch(url,{
                method: "POST"
            });
            dispatch({
                type: ACTION_TYPES.GET_ROOT_DATA,
                payload :{ gradeData: _.get(responce,'data',{})}
            })
            dispatch({
                type: ACTION_TYPES.SET_TEST_DATA,
                payload :{ testData: _.get(responce,'testCountData',{})}
            })
        }
        catch (e) {
            console.log(e)
        }

    }
}

export function refreshData(){

    return async (dispatch, getState,{fetch}) => {

        try{
            const url = API_URLS.GET_DETAILS;
            const responce = await fetch(url, {
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });

            dispatch({
                type: ACTION_TYPES.SET_USER_DETAILS,
                payload : {data: responce},
                
            })
        }
        catch (e) {
            console.log(e)
        }

    }
}


export function doSignup(data){

    return async (dispatch, getState,{fetch}) => {

        try{
            const url = API_URLS.DO_SIGNUP;
            const responce = await fetch(url, {
                body: jsonToFormEncorded(data),
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });
            return responce
        }
        catch (e) {
            console.log(e)
        }

    }
}

export function doLogin(data){

    return async (dispatch, getState,{fetch}) => {

        try{
            const url = API_URLS.DO_LOGIN;
            const responce = await fetch(url, {
                body: `id=${data.username}&password=${data.password}`,
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST"
            });
            dispatch({
                type: ACTION_TYPES.SET_USER_DETAILS,
                payload : {data:_.get(responce,'data',{})}
            })
            return responce
        }
        catch (e) {
            console.log(e)
        }

    }
}