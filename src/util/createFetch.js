



/**
 * Creates a wrapper function around the HTML5 Fetch API that provides
 * default arguments to fetch(...) and is intended to reduce the amount
 * of boilerplate code in the application.
 * https://developer.mozilla.org/docs/Web/API/Fetch_API/Using_Fetch
 */

export class TokenProvider{
    static token;
    static setToken(token) {
        this.token = token;
    }
    static getToken(){
        if(this.token){ 
         return this.token
        }
        return false;
    }
}

function createFetch(fetch, getCookie) {
    // NOTE: Tweak the default options to suite your application needs
    return async (url,data) => {
        const cookie =  TokenProvider.getToken();
        const ck = cookie? `${cookie.key}=${cookie.value}`:'';
        return fetch(url,{...data,headers: {...data.headers,'Cookie': ck}}).then(response => response.json())
            .catch(e => console.error)
    }
}

export default createFetch;
