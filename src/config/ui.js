export const colors =  {
    BLUE: '#4267b2',
    textInputBgColor: '#edeff4',
    textInputBorderColor : '#d0d7e9',
    WHITE: '#ffff',
    SUCCESS: "#009925",
    accentBlue: '#4285f4',
    PINK: '#eb5c78',
    GREEN_BLUE: '#1995b7',
    PLAY_YELLOW: '#fbb305',
    ORANGE: '#fe963b',
    YELLOW: '#f5de4a'
}

export const fontSizes = {
    LARGE: 25,
    MEDIUM: 20,
    Small: 15
}