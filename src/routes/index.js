import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import AuthLoadingScreen from '../Auth/AuthLoadingScreen'
import SignupLandingPage from '../Screen/SignupLandingPage/SignupLandingPage'
import LoginLandingPage from '../Screen/Login/LoginLandingPage';
import home from '../Screen/Home/home';
import TestSelection from '../Screen/TestSelection/test_selection';
import game from '../Screen/Game/game';
import postGame from '../Screen/PostGame/postGame';


const AuthStack = createStackNavigator({SignIn:{ screen: LoginLandingPage },SignUp:{ screen: SignupLandingPage } },{ initialRouteName: 'SignIn'});
const AppStack = createStackNavigator({Home:{ screen: home },Game:{ screen: game },TestSelection:{ screen: TestSelection } ,PostGame:{ screen: postGame }  },{ initialRouteName: 'Home'});

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
     App: AppStack,
     Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
))