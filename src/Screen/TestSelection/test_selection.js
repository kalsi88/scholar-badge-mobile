import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash'
import { IMAGE_URLS } from '../../config/URLconfig';
import {View,Text,Image,StyleSheet,ImageBackground} from 'react-native'
import { fontSizes , colors} from '../../config/ui';
import CardPicker from '../../component/CardPicker/card_picker';
import Acordion from '../../component/Acordion/acordion';
import { setSelectedUserData } from '../../action/CommonActions';
import {getTestDataForChapter} from '../../action/TestActions';
import { ScrollView } from 'react-native-gesture-handler';
import { showLoader } from '../../util/coreUtils';

class TestSelection extends React.Component{

    static navigationOptions = {
        title: 'Home',
        headerStyle: {
            backgroundColor: colors.BLUE,
          },
          headerTintColor: '#fff',
        headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: fontSizes.LARGE,
        textAlign: 'center',
        opacity: .4,
        flexGrow:1,
        alignSelf:'center',
    },

      };
    constructor(props){
        super(props);
        this.subjects = _.get(_.find(props.grades,['_id',props.selectedData.gradeID]),'subjectList');
        this.state = {
            playTest: false,
            testComplete: false,
            chapters: [],
            subject: ''
        }

    }
    
    subjectChanged = (id,data) => {
        this.props.setSelectedUserData({subjectID:id})
        this.setState({chapters: data.subSubjectList.reduce((acc,crnt) => {
            return acc.concat(crnt.chapterList);
        },[]),subject: [id,data]})
    }

    startTest = async (testData,mappedChapterId) => {

        if(testData.played){
            console.log("TODO Improvment test");
        }else{
            await showLoader(this,true)
            //const mcqData = await startA_NewTest(mappedChapterId)
            const mcqData =  {"status":0,"data":{"qList":[{"_id":"5bd04cbe0ff6d407079461b5","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":0},{"_id":"5bd04cbe0ff6d407079461ba","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":1},{"_id":"5bd04cbe0ff6d407079461bf","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":2},{"_id":"5bd04cbe0ff6d407079461c4","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":0},{"_id":"5bd04cbe0ff6d407079461c9","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":1},{"_id":"5bd04cbe0ff6d407079461ce","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":2},{"_id":"5bd04cbe0ff6d407079461d3","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":0},{"_id":"5bd04cbe0ff6d407079461d8","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":1},{"_id":"5bd04cbe0ff6d407079461dd","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":2},{"_id":"5bd04cbe0ff6d407079461e2","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":0},{"_id":"5bd04cbe0ff6d407079461e7","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":1},{"_id":"5bd04cbe0ff6d407079461ec","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":2},{"_id":"5bd04cbe0ff6d407079461f1","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":0},{"_id":"5bd04cbe0ff6d407079461f6","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":1},{"_id":"5bd04cbe0ff6d407079461fb","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":2},{"_id":"5bd04cbe0ff6d40707946200","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":0},{"_id":"5bd04cbe0ff6d40707946205","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":1},{"_id":"5bd04cbe0ff6d4070794620a","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":2},{"_id":"5bd04cbe0ff6d4070794620f","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":0},{"_id":"5bd04cbe0ff6d40707946214","ques":"Question?","optionList":[{"_id":"5bd04cbe0ff6d407079461b1","text":"Option - 1(Correct)","isCorrect":true},{"_id":"5bd04cbe0ff6d407079461b2","text":"Option - 2","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b3","text":"Option - 3","isCorrect":false},{"_id":"5bd04cbe0ff6d407079461b4","text":"Option - 4","isCorrect":false}],"difficulty":1}],"testId":"5d4f0b7cc1bb1a0004458a9f","testType":1}}
           
            await showLoader(this,false)
            this.props.navigation.navigate({routeName:'Game',params:{data:mcqData.data,testName:testData.testName,c_id:mappedChapterId,c_name:testData.c_name}});

        }
    }
    
    
    render() {
        const {
            grades,
            selectedData,
            testMap,
        } = this.props;
        return (
            <View style = {{flex: 1}}>
                <ImageBackground source={IMAGE_URLS.baseBG}  style = {s.mainContainer}>
                    <CardPicker placeholder = {'Select a subject'} value = {this.state.subject} onChange = {this.subjectChanged} list = {this.subjects}></CardPicker>
                    <ScrollView>
                    {
                        
                        this.state.chapters.map((data,index) => {
                            if(!this.props.testMap[data.mappedChapterId]){
                                return
                            }
                            return (
                                <Acordion 
                                    shwoLoader = {(flag) => {showLoader(this,flag)}}  
                                    badgeClicked = {(arg)=> {this.startTest(arg,data.mappedChapterId)}} number = {index + 1}
                                    key = {data._id} 
                                    name = {data.name} 
                                    testData = {this.props.testMap[data.mappedChapterId]}
                                    fetchTestData = {this.props.getTestDataForChapter}
                                    id = {data.mappedChapterId}/>

                            )
                        })
                    }
                    </ScrollView>
                </ImageBackground>
            </View>
        );

    }


}

const s = StyleSheet.create({
    mainContainer: {
        padding: 25,
        height: '100%',
    },
    
})
const mapStateToProps = (state) => {
    return {
        grades: state.CommonData.grades,
        selectedData: state.selectedData,
        testMap: state.testData.testMap
    }
};

const mapDispatchToProps = {
    setSelectedUserData,
    getTestDataForChapter
};

export default connect(mapStateToProps, mapDispatchToProps)(TestSelection);
