import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash'
import {doLogin} from "../../action/CommonActions";
import { IMAGE_URLS } from '../../config/URLconfig';
import {View,Text,Image,StyleSheet,AsyncStorage} from 'react-native'
import { fontSizes , colors} from '../../config/ui';
import Input from '../../component/Input/input';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Button from '../../component/Button/button';
import { showLoader } from '../../util/coreUtils';

class SignupLandingPage extends React.Component{

    static navigationOptions = {
        title: 'Login',
        headerStyle: {
            backgroundColor: colors.BLUE,
          },
          headerTintColor: '#fff',
        headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: fontSizes.LARGE,
        textAlign: 'center',
        flex:1,
        opacity: .4,
        alignSelf:'center',
    },

      };
    constructor(props){
        super(props);
        this.state = {};

    }
    forgotPassword = () => {

    }
    submit = async (data) => {
        showLoader(this,true);
        const resp = await this.props.doLogin(data);
        
        if(Number(_.get(resp,'status',-500)) > -1){
            const cookie = _.get(resp,'cookie');
            await AsyncStorage.setItem('userToken',JSON.stringify(cookie))
            showLoader(this,false)
            this.props.navigation.navigate('App');
        }else{
            showLoader(this,false)
            alert("Something went wrong");
        }
    };

    changeHandler = (key,value) => {
        this.setState({[key]: value});
    }

    launchSignUp = () => {
        this.props.navigation.navigate('SignUp');
    }
    render() {
        const {
            rootData,
        } = this.props;
        return (
            <View style = {{flex: 1}}>
                <View style = {s.mainContainer}>
                    <Image
                        source={IMAGE_URLS.LOGO_URL}
                        style = {s.image}
                    ></Image>
                    <Input
                        placeholder = {"Username or phone number"} 
                        textContentType  ={'username'} 
                        onChange = {(value) => {this.changeHandler('username',value)}}
                        value = {this.state.username}
                            ></Input>
                    <Input
                        style = {{marginTop: 20}}
                        placeholder = {"Password"} 
                        textContentType  ={'newPassword'} 
                        secureTextEntry = {true}
                        onChange = {(value) => {this.changeHandler('password',value)}}
                        value = {this.state.password}
                    ></Input>
                    <TouchableOpacity onPress = {()=> {this.forgotPassword}} >
                        <Text style = {{fontSize : fontSizes.Small,paddingBottom:10,alignSelf: 'flex-end'}}>Forgot Password?</Text>
                    </TouchableOpacity>
                    <Button onClick = {() => this.submit(this.state)}>Sign In</Button>
                </View>
                <View style = {s.bottomButton}>
                    <TouchableOpacity onPress ={this.launchSignUp} style = {{borderTopWidth:1,borderTopColor: colors.textInputBorderColor,padding: 10,}}>
                        <Text style = {{alignSelf:'center'}}>Don't have an account <Text style = {{fontWeight: '600'}}>Sign up</Text></Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        );

    }


}

const s = StyleSheet.create({
    mainContainer: {
        padding: 25,
    },
    bottomButton: {
        flex: 1,
        justifyContent: 'flex-end',
        
    },
    image : {
        alignSelf: 'center',
        marginTop: 70,
        marginBottom:20,
        height: 120,
        width: 120
    },
})
const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = {
    doLogin
};

export default connect(mapStateToProps, mapDispatchToProps)(SignupLandingPage);
